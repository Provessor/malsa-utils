all: options malsa-inc malsa-dec malsa-tog

options:
	@echo malsa-utils build options:
	@echo "LIBS  = -lasound"
	@echo "CC    = ${CC}"

malsa-inc:
	${CC} -o malsa-inc inc.c -lasound

malsa-dec:
	${CC} -o malsa-dec dec.c -lasound

malsa-tog:
	${CC} -o malsa-tog tog.c -lasound

clean:
	rm -f malsa-inc malsa-dec malsa-tog

install:
	cp -f malsa-inc malsa-dec malsa-tog /usr/bin/

uninstall:
	rm -f /usr/bin/malsa-inc /usr/bin/malsa-dec /usr/bin/malsa-tog
