#include <alsa/asoundlib.h>

int
main(int argc, char **argv)
{
	long min, max, curr;
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;

	snd_mixer_open(&handle, 0);
	snd_mixer_attach(handle, "default");
	snd_mixer_selem_register(handle, NULL, NULL);
	snd_mixer_load(handle);

	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, "Master");
	snd_mixer_elem_t *elem = snd_mixer_find_selem(handle, sid);

	snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
	snd_mixer_selem_get_playback_volume(elem, SND_MIXER_SCHN_MONO, &curr);
	snd_mixer_selem_set_playback_volume_all(elem, max / 100 + curr);
	fprintf(stderr, "max: %ld\nmin: %ld\ncurr: %ld\n", max, min, curr);

	snd_mixer_close(handle);

	/**
	FILE *f;
	f = fopen("/tmp/dstatus.fifo", "w+");
	fprintf(f, "a");
	fclose(f);
	/**/
}
